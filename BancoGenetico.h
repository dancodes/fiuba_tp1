#pragma once
#include <iostream>
#include "lista.h"
#include "NodoBanco.h"

class informacionGenetica;

class BancoGenetico {
    public:

    /*
        pre: el gen a agregar no debe estar peresente en la lista de genes
        post:Agrega un Gen nuevo a la lista de genes
    */
        static void agregarGen(informacionGenetica* gen, unsigned int turno_actual);

    /*
        pre:el gen debe existir en la lista
        post:devuelve la edad del gen
        si el gen no existe, devuelve -1
    */
        static unsigned int obtenerEdad(informacionGenetica* gen, unsigned int turno_actual);
    /*
        pre: el gen debe existir en la lista
        post:devuelve un puntero al gen pedido
        si el gen no exite, devuelve un puntero apuntando a NULL
    */
        static informacionGenetica* obtenerGen(std::string bits);

    /*
        pre:
        post:elimina todos los genes de la lista
    */
        static void borrarGenes();

        static Lista<NodoBanco*>* genes;

    private:
        static NodoBanco* buscarGen(informacionGenetica* gen);


};
