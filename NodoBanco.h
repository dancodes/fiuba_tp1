#include "informacionGenetica.h"

class NodoBanco {
    public:
        NodoBanco(informacionGenetica* gen, unsigned int turno_inicial);
        unsigned int obtenerEdad(unsigned int turno_actual);
        informacionGenetica* obtenerGen();
    private:
        informacionGenetica* gen;
        unsigned int turno_inicial;
};
