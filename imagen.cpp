#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <sstream>
#include "imagen.h"
#include "tablero.h"

using namespace std;

void Imagen::dibujarTablero(Tablero* t, string nombre) {
    int columnas = t->obtenerTamCol();
    int filas = t->obtenerTamFil();

    int escala = Imagen::determinarEscala(filas,columnas);
    int spx = 0;
    int spy = 0;
    BMP imagen;

    imagen.SetSize(columnas * escala, filas * escala);

    for (int y = 0; y < filas; y++) {
        for (int x = 0; x < columnas; x++) {


            RGBApixel color;

            if(t->obtenerCelula(y+1,x+1)->obtenerEstado() == 1) {
                color = Imagen::color(255,24,24);
            } else {
                color = Imagen::color(225,225,225);
            }

            Imagen::dibujarCelula(spx, spy, escala, &imagen, &color);

            /*
               for (int i = 0; i < escala; i++) {
               for (int j = 0; j < escala; j++) {
               imagen.SetPixel(spy+i, spx+j, color);
               }
               }*/


            spx = spx + escala;

            if(spx >= columnas * escala) spx = 0;

        }

        spy = spy + escala;
        if(spy >= filas * escala) spy = 0;
    }

    string carpeta = "imagenes/";
    string salida = carpeta + nombre + ".bmp";

    imagen.WriteToFile(salida.c_str());
}

RGBApixel Imagen::color(int r, int g, int b) {
    RGBApixel p;

    p.Red = r;
    p.Green = g;
    p.Blue = b;
    p.Alpha = 1;

    return p;
}

void Imagen::dibujarCelula(int x, int y, int escala, BMP* imagen, RGBApixel* color) {

    //Empezamos desde el 0. El siguiente pixel estaria en escala -1
    //Ej: si escala = 20, x1 = 0, x2 = 19
    int x_fin = x + escala - 1;
    int y_fin = y + escala - 1;


    for(int i = x; i <= x_fin; i++) {
        for(int j = y; j <= y_fin; j++) {

            int pos_x = (i - x + 1);
            int pos_y = (j - y + 1);

            int k_celula = Imagen::calularKcelula(pos_x, pos_y, escala);

            int centro = std::ceil((double)(escala) / 2);
            int k_maximo = Imagen::calularKcelula(centro, centro, escala);

            double brillo_escala = (double)k_celula / (double)k_maximo;

            int sumando = 150 * brillo_escala;

            RGBApixel nuevo_color = *color;


            if(color->Red == 225 &&
                    color->Green == 225 &&
                    color->Blue && 225) {

                sumando = 100 *  brillo_escala;

            }

            nuevo_color = Imagen::ajustarBrillo(color, sumando);

            if(pos_x == 1 || pos_y == 1) {
                nuevo_color = Imagen::colorDelBorde(escala);
            }

            imagen->SetPixel(i,j, nuevo_color);

        }
    }


}

RGBApixel Imagen::ajustarBrillo(RGBApixel* original, int sumando) {
    RGBApixel p;

    int n_rojo = Imagen::aumentarNumero(original->Red, sumando);
    int n_verde = Imagen::aumentarNumero(original->Green, sumando);
    int n_azul = Imagen::aumentarNumero(original->Blue, sumando);

    p.Red = n_rojo;
    p.Green = n_verde;
    p.Blue = n_azul;

    return p;
}

int Imagen::aumentarNumero(int original, int sumando) {
    int suma = original + sumando;

    if(suma > 255) return 255;
    else if (suma < 0) return 0;
    else return suma;
}

int Imagen::calularKcelula(int x, int y, int escala) {
    int centro_x = std::ceil((double)(escala) / 2);

    int dist_infinita = std::max(std::abs(x - centro_x),std::abs(y - centro_x));
    // int distancia_al_centro = std::abs(centro_x - dist_infinita);

    return centro_x - dist_infinita;
}

int Imagen::determinarEscala(int ancho, int alto) {


    int max_ancho = 1024;
    int max_alto = 968;

    int escala = std::floor((double)max_ancho / (double)ancho);

    if(escala * alto > max_alto) {
        escala = max_alto / alto;
    }

    return escala;
}

RGBApixel Imagen::colorDelBorde(int escala) {
    RGBApixel base = Imagen::color(200,200,200);

    RGBApixel nuevo = Imagen::ajustarBrillo(&base, -10*escala);
    return nuevo;

}


void Imagen::dibujarEjes(int tamano_x,int tamano_y,BMP* imagen){

    int eje_y = tamano_x-100;
    int eje_x = 80;
    int borde = 20;
    RGBApixel color= Imagen::color(0,0,0);

    for (int i = borde; i < tamano_x-borde; i++) {
        imagen->SetPixel(i, eje_y, color);
    }

    for(int j= borde; j < tamano_y-borde; j++){
        imagen->SetPixel(eje_x, j, color);
    }
}

void Imagen::dibujarGraficoSeguimiento(SeguimientoGen* seg) {

    int tamano_x = 1000;
    int tamano_y = 1000;
    int margen = 100;
    int turno_actual = 0;
    RGBApixel color_linea = Imagen::color(255,0,0);

    BMP imagen;
    imagen.SetSize(tamano_x, tamano_y);

    Imagen::fondoBlanco(tamano_x,tamano_y,&imagen);
    //  Imagen::dibujarCuadrado(margen_borde,margen_borde,tamano_x - margen_borde,tamano_y - margen_borde, color_linea, &imagen);
    Imagen::dibujarEjes(tamano_x,tamano_y,&imagen);

    int espacio_x = tamano_x - 2*margen - 2; //Dos pixeles por el borde :)
    int espacio_y = tamano_y - 2*margen - 2;
    int espacio_x_por_turno = std::ceil((float)espacio_x / (float) seg->cantidadDeRegistros());

    double espacio_y_factor = (double)espacio_y / (double) seg->valorMaximo();

    int x_inicio = margen + 1;
    int y_inicio = tamano_y - margen - 1;

    seg->iniciarCursor();
    while(seg->avanzarCursor()) {

        int valor = seg->obtenerCursor();

        int x_fin = x_inicio + espacio_x_por_turno;
        int y_fin = tamano_y - margen - 1 - std::ceil(espacio_y_factor * (double)valor);



        cout << seg->obtenerCursor() << endl;
        cout << "x: " << x_inicio << " - y: " << y_inicio << endl;
        cout << "x: " << x_fin << " - y: " << y_fin << endl;

        if(turno_actual != 0) {
            Imagen::Bresenham(x_inicio,y_inicio,x_fin,y_fin,&imagen, color_linea);
            x_inicio = x_fin;
        }

        y_inicio = y_fin;

        turno_actual++;
    }

    string carpeta = "imagenes/";

    string turno_inicial = Imagen::int2String(seg->obtenerTurnoInicial());
    string turno_final = Imagen::int2String(seg->obtenerTurnoFinal());

    string salida = carpeta + "gen-" + seg->obtenerGen()->obtenerBits() + "-" + turno_inicial + "-" + turno_final +  ".bmp";

    imagen.WriteToFile(salida.c_str());
}

string Imagen::int2String(int n) {
    std::ostringstream os;
    os << n;
    return os.str();
}

void Imagen::fondoBlanco(int max_x, int max_y, BMP* imagen) {
    for(int i = 0; i < max_x; i++) {
        for(int j = 0; j < max_y; j++) {
            imagen->SetPixel(i,j,Imagen::color(255,255,255));
        }
    }
}

void Imagen::dibujarCuadrado(int x1, int y1, int x2, int y2, RGBApixel &color, BMP* imagen) {

    for(int i = x1; i <= x2; i++) {
        for(int j = y1; j <= y2; j++) {

            if( (i == x1 || i == x2) || (j == y1 || j == y2) ) {
                imagen->SetPixel(i,j, color);
                // cout << "Pixel!" << i << " " << j << endl;
            }
        }
    }
}


void Imagen::Bresenham(int x1, int y1, int const x2, int const y2, BMP* imagen, RGBApixel &color) {
    // {{{
    int delta_x = x2 - x1;
    // if x1 == x2, then it does not matter what we set here
    signed char const ix = (delta_x > 0) - (delta_x < 0);
    delta_x = std::abs(delta_x) << 1;

    int delta_y = y2 - y1;
    // if y1 == y2, then it does not matter what we set here
    signed char const iy = (delta_y > 0) - (delta_y < 0);
    delta_y = std::abs(delta_y) << 1;

    imagen->SetPixel(x1,y1,color);

    if (delta_x >= delta_y)  {
        // error may go below zero
        int error = delta_y - (delta_x >> 1);

        while (x1 != x2)
        {
            if ((error >= 0) && (error || (ix > 0))) {
                error -= delta_x;
                y1 += iy;
            }
            // else do nothing

            error += delta_y;
            x1 += ix;

            imagen->SetPixel(x1,y1,color);
        }
    } else {
        // error may go below zero
        int error = delta_x - (delta_y >> 1);

        while (y1 != y2) {
            if ((error >= 0) && (error || (iy > 0))) {
                error -= delta_y;
                x1 += ix;
            }
            // else do nothing

            error += delta_x;
            y1 += iy;

            imagen->SetPixel(x1,y1,color);
        }
    }
    // }}}
}


