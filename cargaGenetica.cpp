#include "cargaGenetica.h"

cargaGenetica::cargaGenetica(int intensidad, informacionGenetica* info) {
    this->intensidad = intensidad;
    this->info = info;
}

void cargaGenetica::cambiarIntensidad(int intensidad){
    this->intensidad = intensidad;
}

int cargaGenetica::obtenerEdad(){
    return this->edad;
}

int cargaGenetica::obtenerIntensidad(){
    return this->intensidad;
}

informacionGenetica* cargaGenetica::obtenerInfo(){
    return this->info;
}
