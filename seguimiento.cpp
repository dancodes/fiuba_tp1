#include "seguimiento.h"

SeguimientoGen::SeguimientoGen(informacionGenetica* gen, int turno_inicial) {
    this->p_gen = gen;

    this->intensidades = new Lista<int>;

    this->valor_maximo = 0;
    this->turno_inicial = turno_inicial;
}

SeguimientoGen::~SeguimientoGen() {
    delete this->intensidades;
}

void SeguimientoGen::registrarTurno(int n) {
    this->intensidades->agregar(n);

    if(n > this->valor_maximo) {
        this->valor_maximo = n;
    }
}

void SeguimientoGen::terminarSeguimiento(int turno_final) {
    this->turno_final = turno_final;
}

void SeguimientoGen::iniciarCursor() {
    this->intensidades->iniciarCursor();
}

bool SeguimientoGen::avanzarCursor() {
    return this->intensidades->avanzarCursor();
}

int SeguimientoGen::obtenerTurnoInicial() {
    return this->turno_inicial;
}

int SeguimientoGen::obtenerTurnoFinal() {
    return this->turno_final;
}

int SeguimientoGen::obtenerCursor() {
    return this->intensidades->obtenerCursor();
}

unsigned int SeguimientoGen::valorMaximo() {
    return this->valor_maximo;
}

unsigned int SeguimientoGen::cantidadDeRegistros() {
    return this->intensidades->contarElementos();
}

informacionGenetica* SeguimientoGen::obtenerGen() {
    return this->p_gen;
}
