#include <iostream>
#include "celula.h"

using namespace std;

celula::celula() {
}

/*
 * Genera una celula a partir de su carga genetica
 */
celula::celula(Lista<cargaGenetica*>* &carga) {
    this->estado = 1;

    //casteamos para sacar el const
    // Lista<cargaGenetica*>* plista = const_cast<Lista<cargaGenetica*>*>(carga);
    this->cargasGeneticas.copiarLista(carga);
}

/*
 * Genera una celula a partir de sus vecinas
 */
celula::celula(Lista<celula*>* &vecinas, int turno_actual) {
    this->estado = 1;

    //Transferencia
    Lista<cargaGenetica*>* cargas = new Lista<cargaGenetica*>; //Unica lista con todas las cargas geneticas
    Lista<cargaGenetica*>* cargasDistintas = new Lista<cargaGenetica*>; //Todas las cargas distintas que deben transferirse

    this->generarListaUnica(vecinas,cargas); //Guardo todas las cargas en una sola lista
    this->procesoDeTransferencia(cargas, cargasDistintas, turno_actual);

    this->cargasGeneticas = *cargasDistintas;

    //Mutacion
    Lista<cargaGenetica*>* cargasDesactivadas = new Lista<cargaGenetica*>;
    this->agregarCargasDesactivadas(cargasDistintas,cargasDesactivadas);

    if(!cargasDesactivadas->estaVacia()){
        cargaGenetica* mutacion = this->realizarMutaciones(cargasDesactivadas);
        this->cargasGeneticas.agregar(mutacion);
        cout<<"[+]Se ha agregado una mutacion";
        cout<<" con esta secuencia de bits:"<<mutacion->obtenerInfo()->obtenerBits()<<endl;

    }

    //delete cargas;
    //delete cargasDistintas;

}

void celula::generarListaUnica(Lista<celula*>* &vecinas, Lista<cargaGenetica*>* cargas){
    vecinas->iniciarCursor();

    while(vecinas->avanzarCursor()){
        celula* ce = vecinas->obtenerCursor(); //Obtengo cada celula vecina
        this->generarListaUnica(ce, cargas);
    }
}

void celula::generarListaUnica(celula* ce, Lista<cargaGenetica*>* cargas) {
    Lista<cargaGenetica*> cargasPorCelula = ce->cargasGeneticas;
    cargasPorCelula.iniciarCursor();
    while(cargasPorCelula.avanzarCursor()){
        cargaGenetica* unaCarga = cargasPorCelula.obtenerCursor();//Obtengo las cargas geneticas de cada celula vecina
        cargas->agregar(unaCarga); //Las agrego a la lista general con todas las cargas geneticas
    }
}

void celula::procesoDeTransferencia(Lista<cargaGenetica*>* cargas, Lista<cargaGenetica*>* cargasDistintas, int turno_actual){

    cargas->iniciarCursor();
    while(cargas->avanzarCursor()){
        cargaGenetica* unaCarga = cargas->obtenerCursor();
        aplicarReglasTransferencia(unaCarga, cargas, cargasDistintas, turno_actual); //Busco gen en la lista general y las agrego en la lista con cargas distintas
    }
}

void celula::aplicarReglasTransferencia(cargaGenetica* unaCarga, Lista<cargaGenetica*>* cargas, Lista<cargaGenetica*>* cargasDistintas, int turno_actual){

    informacionGenetica* info = unaCarga->obtenerInfo();

    int cantidadGenesIguales = 0;
    int cargasDesactivadas = 0;
    int pos = 1;
    int maximaIntensidad = unaCarga->obtenerIntensidad();
    int totalIntensidades = unaCarga->obtenerIntensidad();


    cargas->iniciarCursor();
    while(cargas->avanzarCursor() && cantidadGenesIguales <= 3){
        cargaGenetica* cargaOtra = cargas->obtenerCursor();
        informacionGenetica* infoOtra = cargaOtra->obtenerInfo();

        if(info->esIgualA(infoOtra)){

            cantidadGenesIguales++;
            cargas->remover(pos); //Lo saco de la lista general para no volver a contarlo
            pos--;

            if(maximaIntensidad < cargaOtra->obtenerIntensidad()){
                maximaIntensidad = cargaOtra->obtenerIntensidad();
            }
            if(cargaOtra->obtenerIntensidad() == 0){
                cargasDesactivadas++;
            }

            totalIntensidades = totalIntensidades + cargaOtra->obtenerIntensidad();
        }
        pos++;
    }

    this->transferencia(unaCarga, cargasDistintas,cantidadGenesIguales, maximaIntensidad, cargasDesactivadas, totalIntensidades, turno_actual);
}

void celula::transferencia(cargaGenetica* unaCarga, Lista<cargaGenetica*>* cargasDistintas,
        int cantidadGenesIguales, int maximaIntensidad,int cargasDesactivadas,
        int totalIntensidades, int turno_actual){


    int edad_carga = BancoGenetico::obtenerEdad(unaCarga->obtenerInfo(), turno_actual);

    if(cantidadGenesIguales==3){
        if(cargasDesactivadas>0){
            unaCarga->cambiarIntensidad(maximaIntensidad);
        } else {
            int promedioTurnos = ((edad_carga / turno_actual)*100)+1;
            unaCarga->cambiarIntensidad(promedioTurnos);

        }
    } else if(cantidadGenesIguales==2){

        if(cargasDesactivadas==0){
            int promedioIntensidades = totalIntensidades / 2;
            unaCarga->cambiarIntensidad(promedioIntensidades);
        } else if(cargasDesactivadas==1){
            unaCarga->cambiarIntensidad(maximaIntensidad); //Es la unica por lo tanto la maxima
        } else {
            int promedioTurnos = ((edad_carga / turno_actual)*100)+1;
            unaCarga->cambiarIntensidad(promedioTurnos);
        }
    }

    cargasDistintas->agregar(unaCarga);
}

void celula::agregarCargasDesactivadas(Lista<cargaGenetica*>* cargasDistintas,Lista<cargaGenetica*>* cargasDesactivadas){
    cargasDistintas->iniciarCursor();
    while(cargasDistintas->avanzarCursor()){
        cargaGenetica* unaCarga = cargasDistintas->obtenerCursor();
        if(unaCarga->obtenerIntensidad() == 0){
            cargasDesactivadas->agregar(unaCarga);
        }
    }
}


cargaGenetica* celula::realizarMutaciones(Lista<cargaGenetica*>* cargasDesactivadas){

    int cargaMinima;
    int edadMinima;
    int promedio;

    cargasDesactivadas->iniciarCursor();
    cargasDesactivadas->avanzarCursor();
    cargaGenetica* cargaconMutacion =  cargasDesactivadas->obtenerCursor();
    informacionGenetica* infoMutacion = cargaconMutacion->obtenerInfo();

    cargaMinima = cargaconMutacion->obtenerIntensidad();
    edadMinima =  cargaconMutacion->obtenerEdad();

    while(cargasDesactivadas->avanzarCursor()){
        cargaGenetica* unaCargaDesactivada = cargasDesactivadas->obtenerCursor();
        informacionGenetica* infoGen = unaCargaDesactivada->obtenerInfo();
        infoMutacion->combinarCon(infoGen); //Mutacion del Primero con todos
        if(unaCargaDesactivada->obtenerEdad()<cargaMinima){
            cargaMinima = unaCargaDesactivada->obtenerEdad();
        }
    }

    promedio = ((cargaMinima/edadMinima)*100)+1;

    cargaconMutacion->cambiarIntensidad(promedio);
    return cargaconMutacion;
}


int celula::obtenerEstado(){
    return this->estado;
}

void celula::cambiarEstado(int estado){
    this->estado = estado;
}


celula::~celula() {

}

