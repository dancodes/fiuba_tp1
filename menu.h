#include <fstream>
#ifndef MENU_H_
#define MENU_H_

using namespace std;

class Menu{
    public:
        /*PRE: -
        POST: Pide al usuario el nombre del archivo
        */
        string pedirNombreArchivo();

        /*PRE: Bits validos
        POST: Pide al usuario los bits del gen
        */
        string pedirBitsGen();

        /*PRE: -
        POST: Muestra las opciones del MENU */
        void mostrarOpciones();

        /* PRE: -
        POST: Muestra el logo del juego de la vida */
        void mostrarLogo();

        /* PRE: Opcion valida
        POST: Pide al usuario la opcion deseada
        */
        int elegirOpcion();

        /* PRE: -
        POST: Imprime saltos de linea para mejorar la interfase
        */
        void imprimirSaltos(int saltos);
    private:
        string nombreArchivo;
};

#endif
