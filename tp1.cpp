#include <iostream>
#include <sstream>
#include "lista.h"
#include "tablero.h"
#include "imagen.h"
#include "menu.h"

using namespace std;


int main()
{

    int opcion;
    string nombreArchivo;

    Menu Juego;
    Juego.mostrarLogo();
    nombreArchivo = Juego.pedirNombreArchivo();

    Tablero tablero(nombreArchivo);

    do {
        string bits_gen_pedido;

        Juego.imprimirSaltos(3);
        Juego.mostrarLogo();
        Juego.imprimirSaltos(6);
        Juego.mostrarOpciones();
        cout<<"[+] Cantidad de turnos ejecutados: "<<tablero.obtenerTurno()<<endl<<endl;

        stringstream archivo_imagen;
        archivo_imagen << "salida_" << tablero.obtenerTurno();

        Imagen::dibujarTablero(&tablero, archivo_imagen.str());

        opcion=Juego.elegirOpcion();

        Juego.imprimirSaltos(1);

        /* ----- EMI ---------- */
        switch (opcion)
        {
            case 1:
                int i;
                int nroTurnos;
                cout<<"Ingrese cantidad de turnos (1 a 10000): ";
                cin>>nroTurnos;
                if(nroTurnos<1 || nroTurnos>10000){
                    cout<<"Cantidad invalida de turnos"<<endl;
                } else {
                    i=0;
                    while(i<nroTurnos){
                        tablero.ejecutarTurno();
                        stringstream archivo_imagen;
                        archivo_imagen << "salida_" << tablero.obtenerTurno();
                        Imagen::dibujarTablero(&tablero, archivo_imagen.str());
                        i++;
                    }

                    cout<<endl<<"[+] Se han ejecutado "<<i<<" turno/s"<<endl;
                }
                break;
            case 2:
                tablero.reiniciar(nombreArchivo);
                cout<<"El tablero se ha reiniciado"<<endl;
                break;
            case 3:
                bits_gen_pedido = Juego.pedirBitsGen();
                if(tablero.estaEnSeguimiento(bits_gen_pedido)){
                    cout<<"[!] El gen ya se encuentra en seguimiento";
                } else {
                    if(tablero.comprobarSiExisteGen(bits_gen_pedido)){
                        tablero.iniciarSeguimientoGen(bits_gen_pedido);
                        cout<<"[+] El seguimiento del gen '"<<bits_gen_pedido<<"' se ha iniciado con exito!"<<endl;
                    } else {
                        cout<<endl<<"El gen '"<<bits_gen_pedido<<"' no existe en el banco genetico o es invalido";
                    }
                }
                break;
            case 4:
                if(tablero.hayGenesSiguiendo()){
                    tablero.mostrarGenesSiguiendo(); //ERROR: IMPRIME CUALQUIER BIT. SE DEBE ESTAR AGREGANDO MAL
                    cout<<"Ingrese el gen que desea terminar el seguimiento: ";
                    cin>>bits_gen_pedido;
                    if(tablero.comprobarSiExisteGen(bits_gen_pedido)){
                        tablero.detenerSeguimientoGen(bits_gen_pedido);
                        cout<<endl<<"El seguimiento del gen '"<<bits_gen_pedido<<"' ha finalizado";
                    } else {
                        cout<<endl<<"El gen '"<<bits_gen_pedido<<"' no existe en el banco genetico o es invalido";
                    }
                } else {
                    cout<<"[!] No hay ningun gen en seguimiento";
                }
                break;

                // ---- PRUEBA PARA VER QUE HAY EN EL BANCO DE GENES -------
            case 6:
                cout<<"Banco Genetico: "<<endl;
                BancoGenetico::genes->iniciarCursor();
                while(BancoGenetico::genes->avanzarCursor()){
                    NodoBanco* nodo = BancoGenetico::genes->obtenerCursor();
                    cout<<"*"<<nodo->obtenerGen()->obtenerBits()<<endl; // ERROR: NO SE AGREGA EL ULTIMO
                }
                break;
        }

        cout<<endl<<"Presione cualquier tecla para continuar...";
        cin.ignore();
        cin.get();

    } while(opcion!=5);


    BancoGenetico::borrarGenes();


    return 0;
}
