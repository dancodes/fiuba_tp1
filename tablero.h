#include <iostream>
#include <fstream>
#include "BancoGenetico.h"
#include "cargaGenetica.h"
#include "seguimiento.h"
#include "celula.h"

using namespace std;

class Tablero{
    public:
        /* PRE: -
        POST: Crea el tablero con los genes y celulas leidos del archivo */
        Tablero(string nombreArchivo);

        /* PRE: Lee el archivo
           POST: Devuelve true si se puede leer
         */
        bool leerArchivo(string nombre);

        /* PRE: filas>0 & columnas>0
        POST: Inicializa el tablero con las dimensiones del mismo */
        void inicializar(int f, int c);

        /* PRE: -
        POST: Parsea el archivo, agregando celulas, genes
        Define dimensiones del tablero
        */
        void parsearArchivo();

        /* PRE: Tablero creado
        POST: Reinicia el tablero */
        void reiniciar(string nombreArchivo);

        /* PRE: fila>0 col>0
        En caso de existir devuelve un puntero a celula muerta
        POST: Devuelve un puntero a una celula
        */
        celula* obtenerCelula(int f, int c);

        /* PRE: Filas>0 Columnas>0
        POST: Agrega una celula al tablero*/
        void agregarCelula(int f, int c, celula* ce);

        /* PRE: Filas>0 Columnas>0. Celula existente
        POST: Elimina una celula del tablero.
        */
        void removerCelula(int f, int c);

        /* PRE: Celula existente
        POST: Devuelve una lista con las celulas vecinas
        */
        Lista<celula*>* vecinasDeCelula(int f, int c);

        /* PRE: Tablero creado
        POST: Ejecuta un turno en el tablero*/
        void ejecutarTurno();

        /* PRE: Tablero creado
        POST: Limpia el tablero */
        void vaciar();

        /* PRE: Existe en gen
        POST: Comienza a seguir un gen.
        Lo agrega en la lista genes_siguiendo
        */
        void iniciarSeguimientoGen(string bits);



        void procesarTurnoSeguimiento();



        /* PRE: El gen debe estar en seguimiento
        POST: Detiene el seguimiento del gen
        */
        void detenerSeguimientoGen(string bits);

        /* PRE: -
        POST: Obtiene la cantidad de turnos jugados */
        int obtenerTurno();  // NUEVO

        /*PRE: Tablero Inicializado
        POST: Obtiene el tama�o de las columnas */
        int obtenerTamCol();  // NUEVO

        /*PRE: Tablero Inicalizado
        POST: Obtiene el tama�o de las filas */
        int obtenerTamFil();  // NUEVO

        /* PRE: Bits validos
        POST: Devuelve true si existe el gen */
        bool comprobarSiExisteGen(string bits);  // NUEVO

        /* PRE: -
        POST: Muestra los bits de los genes siguiendo */
        void mostrarGenesSiguiendo();  // NUEVO

        /* PRE: -----------------
        POST: */
        bool estaEnSeguimiento(string bits);

        /* PRE: -
        POST: Devuelve TRUE si hay genes en seguimiento
        */
        bool hayGenesSiguiendo();  // NUEVO

        /* PRE: Tablero creado
        POST: Libera la memoria */
        ~Tablero();

        //BancoGenetico Banco; // NUEVO

    private:
        int turno;
        int tamano_c;
        int tamano_f;
        string nombreArchivo;
        ifstream archivo;

        celula*** tablero;
        celula*** tableroBuffer;
        celula celulaMuerta;


        Lista<SeguimientoGen*>* genes_siguiendo;

        /* PRE: Tablero creado
        POST: Copia las celulas del tablero al tablero auxiliar
        */
        void copiarBuffer();
};
