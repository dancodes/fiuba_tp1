#include "NodoBanco.h"

NodoBanco::NodoBanco(informacionGenetica* gen, unsigned int turno_inicial) {
    this->gen = gen;
    this->turno_inicial = turno_inicial;
}

unsigned int NodoBanco::obtenerEdad(unsigned int turno_actual) {
    return turno_actual - this->turno_inicial;
}

informacionGenetica* NodoBanco::obtenerGen() {
    return this->gen;
}
