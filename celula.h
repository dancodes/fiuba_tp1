#ifndef CELULA_H_
#define CELULA_H_
#include "lista.h"
#include "BancoGenetico.h"
#include "cargaGenetica.h"


class celula {
private:
        int estado;
public:
        celula();

		/*
		pre:
		post:Genera una celula a partir de su carga genetica
		*/
        celula(Lista<cargaGenetica*>* &carga_a_copiar);
		/*

		pre: Se deben cumplir que tenga exactamente 3 vecinas vivas
		post: Crea una celula a partir de sus vecinas con sus respectivos genes
		*/
        celula(Lista<celula*>* &vecinas, int turno_actual);


        Lista<cargaGenetica*> cargasGeneticas;

		/*
		pre:
		post:devuelve el estado de la celula
		*/
        int obtenerEstado();

        void generarListaUnica(Lista<celula*>* &vecinas, Lista<cargaGenetica*>* cargas);
        void generarListaUnica(celula* ce, Lista<cargaGenetica*>* cargas);
        void procesoDeTransferencia(Lista<cargaGenetica*>* cargas, Lista<cargaGenetica*>* cargasDistintas, int turno_actual);

		/*
		pre:
		post: Muta y crea los genes a partir de las vecinas de una celula
		*/
        void aplicarReglasTransferencia(cargaGenetica* carga, Lista<cargaGenetica*>* cargas, Lista<cargaGenetica*>* cargasDistintas, int turno_actual);

		/*
		pre: Primero se debe usar el metodo aplicarReglasTransferencia
		post: Transfiere los genes de las vecinas a la nueva celula
		*/
        void transferencia(cargaGenetica* carga,Lista<cargaGenetica*>* cargasDistintas,
                           int cantidadGenesIguales, int maximaIntensidad, int cargasDesactivadas, int totalIntensidades, int turno_actual);

        /* pre: -
        post: cambia el estado de la celula
        */
        void cambiarEstado(int estado);


        void agregarCargasDesactivadas(Lista<cargaGenetica*>* cargasDistintas,Lista<cargaGenetica*>* cargasDesactivadas);
        cargaGenetica* realizarMutaciones(Lista<cargaGenetica*>*  cargasDesactivadas);

        ~celula();

};

#endif
