#ifndef CARGA_H_
#define CARGA_H_
#include "informacionGenetica.h"

class cargaGenetica{
private:
    int edad;
    int intensidad;
    informacionGenetica* info;
public:
    /* PRE: Intesidad>0
    POST: Crea la carga genetica*/
    cargaGenetica(int intensidad, informacionGenetica* info);

    /*PRE: Gen creado
    POST: Cambia la intensidad del gen */
    void cambiarIntensidad(int intensidad);

    /*PRE: Gen creado
    POST: Obtiene la edad del gen */
    int obtenerEdad();

    /*PRE: Gen creado
    POST: Obtiene la intensidad del gen*/
    int obtenerIntensidad();

    /*PRE: Gen creado
    PRE: Devuelve la informacion genetica del gen
    */
    informacionGenetica* obtenerInfo();
};

#endif
