#include <iostream>
#include <fstream>
#include <cstdlib>
#include "lista.h"
#include "tablero.h"
#include "imagen.h"

using namespace std;

Tablero::Tablero(string nombreArchivo){

    this->celulaMuerta.cambiarEstado(0);

    this->turno = 1;

    if(this->leerArchivo(nombreArchivo)) {
        this->parsearArchivo();
    }
    this->copiarBuffer();


    this->genes_siguiendo = new Lista<SeguimientoGen*>;
}

bool Tablero::leerArchivo(string nombreArchivo){

    this->nombreArchivo = nombreArchivo;
    this->archivo.open(this->nombreArchivo.c_str());
    if(!archivo.fail()) {
        return true;
    }  else {
        this->archivo.clear();
        return false;
    }
}

void Tablero::inicializar(int f, int c) {

    this->tablero = new celula**[f];
    this->tableroBuffer = new celula**[f];

    for(int i = 0; i < f; i++) {
        this->tablero[i] = new celula*[c];
        this->tableroBuffer[i] = new celula*[c];
    }

    this->vaciar();

}


void Tablero::reiniciar(string nombreArchivo) {
    this->vaciar();
    this->archivo.close();

    this->turno = 1;

    if(this->leerArchivo(nombreArchivo)) {
        this->parsearArchivo();
    }

    this->copiarBuffer();
}

/*
 * Genera celulas y las agrega al tablero
 */
void Tablero::parsearArchivo() {
    // {{{
    string linea;
    string tablero_f;
    string tablero_c;
    string celula_f;
    string celula_c;
    string gen_b; //informacion genetica
    string gen_i; //intesidad

    Lista<cargaGenetica*>* cargasGeneticas = new Lista<cargaGenetica*>;

    while(this->archivo >> linea){


        if(linea.find("tablero") != string::npos){

            this->archivo >> tablero_f;
            this->tamano_f = atoi(tablero_f.c_str());
            this->archivo >> tablero_c;
            this->tamano_c = atoi(tablero_c.c_str());

            this->inicializar(this->tamano_f, this->tamano_c);

            /* EMI */
        } else if(linea.find("celula") != string::npos) {

            cargasGeneticas = new Lista<cargaGenetica*>;

            this->archivo >> celula_f;
            this->archivo >> celula_c;

            this->archivo >> linea;

            if(linea.find("gen") != string::npos){

                do{
                    this->archivo >> gen_b;
                    this->archivo >> gen_i;

                    informacionGenetica* info_g = new informacionGenetica(gen_b);
                    cargaGenetica* carga_g = new cargaGenetica(atoi(gen_i.c_str()), info_g);
                    BancoGenetico::agregarGen(info_g, this->turno);

                    cargasGeneticas->agregar(carga_g);

                    this->archivo >> linea;
                } while(linea.find("fin") == string::npos);

            }

            celula* ce = new celula(cargasGeneticas);

            this->agregarCelula(atoi(celula_f.c_str()), atoi(celula_c.c_str()), ce);

            delete cargasGeneticas;

        }
        /* - */
    }

    this->archivo.close();
    // }}}
}


celula* Tablero::obtenerCelula(int f, int c) {
    celula* pcel;

    if (f>=1 && c>=1 && f<=this->tamano_f && c<=this->tamano_c) {
        pcel = this->tablero[f-1][c-1];
    } else {
        pcel = 0;
    }

    if(pcel == 0) {
        return &this->celulaMuerta;
    } else {
        return pcel;
    }

}

void Tablero::ejecutarTurno() {

    int nacimientos = 0;
    int muertes = 0;

    for(int i=1; i<=this->tamano_f; i++) {
        for(int j=1; j<=this->tamano_c; j++) {

            Lista<celula*>* vecinas = this->vecinasDeCelula(i,j);
            int vecinas_vivas = vecinas->contarElementos();

            if(this->obtenerCelula(i,j)->obtenerEstado() == 0){ //estado() -> viva o muerta

                if(vecinas_vivas == 3) {
                    cout<<"[+] Se ha creado una celula a partir de sus vecinas en la pos "<<i<<"-"<<j<<endl;
                    celula* nueva_celula = new celula(vecinas, this->turno);
                    this->agregarCelula(i,j,nueva_celula);
                    nacimientos++;
                }

            } else {

                if(vecinas_vivas !=2 && vecinas_vivas != 3) { //Si esta viva y no tiene 2 o 3 vecinos entonces
                    this->removerCelula(i,j);
                    muertes++;
                }
            }

            delete vecinas;
        }
    }

    this->copiarBuffer();

    this->procesarTurnoSeguimiento();

    this->turno++;
}


void Tablero::vaciar() {
    for(int i = 0; i < this->tamano_f; i++) {
        for(int j = 0; j < this->tamano_c; j++) {
            this->tablero[i][j] = 0;
            this->tableroBuffer[i][j] = 0;
        }
    }
}

void Tablero::agregarCelula(int f, int c, celula* ce) {

    int i = f - 1;
    int j = c - 1;

    celula* pcel = this->tableroBuffer[i][j];

    if(pcel != 0) {
        delete pcel;
    }

    this->tableroBuffer[i][j] = ce;

}

Lista<celula*>* Tablero::vecinasDeCelula(int f, int c) {
    Lista<celula*>* celulas_vecinas = new Lista<celula*>;

    for(int i = -1; i <= 1; i++) {
        for(int j = -1; j <= 1; j++) {
            if(!(i == 0 && j == 0)) { //Si no estamos en la mismisima celula...
                if(this->obtenerCelula(f + i,c + j)->obtenerEstado() == 1) {
                    celulas_vecinas->agregar(this->obtenerCelula(f + i,c + j));
                }
            }
        }
    }

    return celulas_vecinas;
}

void Tablero::removerCelula(int f, int c) {

    int i = f -1;
    int j = c -1;

    celula* pcel = this->tableroBuffer[i][j];

    if(pcel != NULL) {
        //delete pcel;
    }

    this->tableroBuffer[i][j] = NULL;

}

void Tablero::copiarBuffer() {
    for(int i = 0; i < this->tamano_f; i++) {
        for(int j = 0; j < this->tamano_c; j++) {
            this->tablero[i][j] = this->tableroBuffer[i][j];
        }
    }
}


void Tablero::iniciarSeguimientoGen(string bits) {

    informacionGenetica* orig = BancoGenetico::obtenerGen(bits);

    SeguimientoGen* seguimiento = new SeguimientoGen(orig, this->turno);

    this->genes_siguiendo->agregar(seguimiento);

    this->procesarTurnoSeguimiento();
}

void Tablero::detenerSeguimientoGen(string bits) {

    this->genes_siguiendo->iniciarCursor();

    int pos = 1;

    while(this->genes_siguiendo->avanzarCursor()) {
        SeguimientoGen* seg = this->genes_siguiendo->obtenerCursor();
        informacionGenetica temp(bits);

        if(seg->obtenerGen()->esIgualA(&temp)) {
            seg->terminarSeguimiento(this->turno);
            //crear imagen
            Imagen::dibujarGraficoSeguimiento(seg);

            delete seg;

            this->genes_siguiendo->remover(pos);
        }
        pos++;
    }
}

void Tablero::procesarTurnoSeguimiento() {

    this->genes_siguiendo->iniciarCursor();
    while(this->genes_siguiendo->avanzarCursor()) {

        SeguimientoGen* seg = this->genes_siguiendo->obtenerCursor();
        int suma = 0;

        //Por cada celula en el tablero, busco sus genes
        //y me fijo si tiene el mismo que uno que se busca
        for(int i = 0; i < this->tamano_f; i++) {
            for(int j = 0; j < this->tamano_c; j++) {

                celula* cel = this->tablero[i][j];

                if(cel != NULL) {
                    cel->cargasGeneticas.iniciarCursor();
                    while(cel->cargasGeneticas.avanzarCursor()) {
                        cargaGenetica* g = cel->cargasGeneticas.obtenerCursor();

                        cout << "GEN: " << seg->obtenerGen()->obtenerBits() << endl;

                        if(g->obtenerInfo()->esIgualA(seg->obtenerGen())) {
                            cout << "Intensidad: " << g->obtenerIntensidad() << endl;
                            suma += g->obtenerIntensidad();

                            cout << "Instancia de gen con intensidad " << g->obtenerIntensidad() << endl;
                        }
                    }
                }
            }

        }

        seg->registrarTurno(suma);
    }

}

int Tablero::obtenerTurno(){
    return this->turno;
}

int Tablero::obtenerTamCol(){
    return this->tamano_c;
}

int Tablero::obtenerTamFil(){
    return this->tamano_f;
}

Tablero::~Tablero() {

    for(int i = 0; i < this->tamano_f; i++) {
        for(int j = 0; j < this->tamano_c; j++) {
            if(this->tablero[i][j] != NULL) {
                delete this->tablero[i][j];
            }
        }

        delete[] this->tablero[i];
        delete[] this->tableroBuffer[i];
    }

    delete[] this->tablero;
    delete[] this->tableroBuffer;

    delete this->genes_siguiendo;
}


bool Tablero::comprobarSiExisteGen(string bits){

    bool valido = true;
    unsigned int i=0;

    while(valido && i<bits.size()){
        if(bits[i] != '1' && bits[i] != '0'){
            valido = false;
        }
        i++;
    }

    return (BancoGenetico::obtenerGen(bits) != NULL && valido);
}

bool Tablero::estaEnSeguimiento(string bits){
    bool encontrado = false;

    this->genes_siguiendo->iniciarCursor();

    while(!encontrado && this->genes_siguiendo->avanzarCursor()){
        SeguimientoGen* pgen_siguiendo = this->genes_siguiendo->obtenerCursor();
        informacionGenetica* gen = pgen_siguiendo->obtenerGen();
        if(gen->obtenerBits() == bits){
            encontrado = true;
        }
    }

    return(encontrado);

}


void Tablero::mostrarGenesSiguiendo(){ //MUESTRA TODO 111 O CUALQUIER COSA. ALGO ESTA CARGANDO MAL EN LA LISTA
    cout<<"[+] Genes siguiendo: "<<endl;
    this->genes_siguiendo->iniciarCursor();
    while(this->genes_siguiendo->avanzarCursor()){
        SeguimientoGen* pgen_siguiendo = this->genes_siguiendo->obtenerCursor();
        informacionGenetica* gen = pgen_siguiendo->obtenerGen();
        cout<<"* Gen:"<<gen->obtenerBits()<<endl;
    }
}

bool Tablero::hayGenesSiguiendo(){
    return (!genes_siguiendo->estaVacia());
}


