#pragma once
#include <iostream>
#include <string>
#include "easybmp/EasyBMP.h"

using namespace std;

class Tablero;
class SeguimientoGen;
class informacionGenetica;

class Imagen {
    public:
        /* PRE: Tablero creado
        POST: Dibuja el tablero con celulas en un archivo BMP
        */

        static void dibujarTablero(Tablero* t, string nombre);

        /*PRE: El gen debe estar en seguimiento
        POST: Dibuja un eje de coordenadas con el seguimiento del gen
        */
        static void dibujarGraficoSeguimiento(SeguimientoGen* seg);

    private:
        static RGBApixel color(int r, int g, int b);
        static void dibujarCelula(int x, int y, int escala, BMP* imagen, RGBApixel* color);
        static int aumentarNumero(int original, int sumando);
        static RGBApixel ajustarBrillo(RGBApixel* original, int sumando);
        static int calularKcelula(int x, int y, int escala);
        static int determinarEscala(int alto, int ancho);
        static RGBApixel colorDelBorde(int escala);
        static void Bresenham(int x1, int y1, int const x2, int const y2, BMP* imagen, RGBApixel &color);
        static void fondoBlanco(int max_x, int max_y, BMP* imagen);
        static void dibujarCuadrado(int x1, int y1, int x2, int y2, RGBApixel &color, BMP* imagen);
        static string int2String(int n);
        static void dibujarEjes(int tamano_x,int tamano_y,BMP* imagen);

};
