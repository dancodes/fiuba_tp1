#include <iostream>
#include "menu.h"

using namespace std;

void Menu::mostrarLogo(){
    cout << " \t    _                                                         "<< endl;
    cout << " \t   |_ |    o      _   _   _     _|  _    |  _.      o  _|  _.  "<< endl;
    cout << " \t   |_ |    | |_| (/_ (_| (_)   (_| (/_   | (_|   \\/ | (_| (_|  "<< endl;
    cout << " \t          _|          _|                                      "<< endl<< endl;
}

void Menu::mostrarOpciones(){

    cout << "[+] Menu:"<< endl;
    cout << "1) Realizar turnos"<< endl;
    cout << "2) Reiniciar juego"<< endl;
    cout << "3) Comenzar seguimiento de un gen" << endl;
    cout << "4) Terminar movimiento de un gen" << endl;
    cout << "5) Salir"<< endl<< endl;
}

string Menu::pedirBitsGen(){

    string bits;

    cout << "[-] Ingrese el string de bits del gen (ej: 100010010)" << endl;
    cin >> bits;

    return bits;

}

void Menu::imprimirSaltos(int saltos){
    for(int i=0; i<=saltos;i++){
        cout<<endl;
    }
}

string Menu::pedirNombreArchivo(){

    ifstream archivo;
    bool exito = false;
    do {
        cout <<"[-] Ingrese el nombre del archivo"<< endl;
        cout <<"Ejemplo: 'celulas.txt'"<< endl;
        cout <<"Ruta: ";
        cin >> this->nombreArchivo;

        archivo.open(this->nombreArchivo.c_str());
        if(archivo.fail()) {
            cout <<"[!] Error abriendo el archivo. " << endl << endl;
            exito = false;
            archivo.clear();
        }
        else {
            exito = true;
        }
    } while (!exito);

    return this->nombreArchivo;
}

int Menu::elegirOpcion() {
    int opcion;
    cout << "[-] Elija una opcion" << endl;
    cin >> opcion;

    return opcion;
}
