#include <iostream>
#include "informacionGenetica.h"

using namespace std;

informacionGenetica::informacionGenetica(string bits){
            this->bits = bits;
}

string informacionGenetica::invertirBits(string bits){

    string bitsInvertidos;

    for(int i=bits.size();i>=0; i--){
        bitsInvertidos += bits[i];
    }

    return bitsInvertidos;
}

bool informacionGenetica::estaEncendidoBit(unsigned int pos){
    string bits;

    bits = this->invertirBits(this->bits);

    return (this->bits[pos] == '1');
}

unsigned int informacionGenetica::contarBits(){
    return this->bits.size();
}

bool informacionGenetica::esIgualA(informacionGenetica* otra){

    bool esIgual = true;

    if(this->contarBits() != otra->contarBits()){
        esIgual = false;
    } else {
        unsigned int i=0;
        while(esIgual == true && i<contarBits()){
            if(this->estaEncendidoBit(i) != otra->estaEncendidoBit(i)){
                esIgual = false;
            }
            i++;
        }
    }

    return esIgual;
}

void informacionGenetica::combinarCon(informacionGenetica* otra){

    string mutacion;
    unsigned int i=0;

    while(i<this->contarBits() && i<otra->contarBits()){
        if(this->estaEncendidoBit(i) || otra->estaEncendidoBit(i)){
            mutacion += "1";
        } else {
            mutacion += "0";
        }
        i++;
    }

    while(i<this->contarBits()){
        if(this->estaEncendidoBit(i)){
            mutacion += "1";
        } else {
            mutacion += "0";
        }
        i++;
    }

    while(i<otra->contarBits()){
        if(otra->estaEncendidoBit(i)){
            mutacion += "1";
        } else {
            mutacion += "0";
        }
        i++;
    }

    this->bits = mutacion;

}


string informacionGenetica::obtenerBits() {
    return this->bits;
}
