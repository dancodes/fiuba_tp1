#include <iostream>
#include "BancoGenetico.h"

using namespace std;

Lista<NodoBanco*>* BancoGenetico::genes = new Lista<NodoBanco*>;

void BancoGenetico::agregarGen(informacionGenetica* gen, unsigned int turno_actual) {

    if(BancoGenetico::buscarGen(gen) == NULL) {
        NodoBanco* nodo = new NodoBanco(gen, turno_actual);
        BancoGenetico::genes->agregar(nodo);
    }
}

NodoBanco* BancoGenetico::buscarGen(informacionGenetica* gen) {

    bool encontrado = false;
    NodoBanco* nodo_encontrado = NULL;

    if(!BancoGenetico::genes->estaVacia()) {
        BancoGenetico::genes->iniciarCursor();

        while(BancoGenetico::genes->avanzarCursor() && !encontrado) {
            NodoBanco* nodo = BancoGenetico::genes->obtenerCursor();

            if (nodo->obtenerGen()->esIgualA(gen)) {
                encontrado = true;
                nodo_encontrado = nodo;
            }
        }
    }

    return nodo_encontrado;
}

informacionGenetica* BancoGenetico::obtenerGen(std::string bits) {

    informacionGenetica gen(bits);

    NodoBanco* nodo = BancoGenetico::buscarGen(&gen);

    if(nodo != NULL) {
        return nodo->obtenerGen();
    } else {
        return NULL;
    }
}

unsigned int BancoGenetico::obtenerEdad(informacionGenetica* gen, unsigned int turno_actual) {
    NodoBanco* nodo = BancoGenetico::buscarGen(gen);

    if(nodo != NULL) {
        return nodo->obtenerEdad(turno_actual);
    } else {
        return -1;
    }
}



void BancoGenetico::borrarGenes() {
    BancoGenetico::genes->iniciarCursor();

    while(BancoGenetico::genes->avanzarCursor()) {
        NodoBanco* nodo = BancoGenetico::genes->obtenerCursor();

        delete nodo->obtenerGen();
        delete nodo;
    }

    delete BancoGenetico::genes;
}

