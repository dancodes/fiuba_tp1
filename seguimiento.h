#include "lista.h"
#include "informacionGenetica.h"

class cargaGenetica;
class informacionGenetica;

class SeguimientoGen {
    public:
        /* PRE: Turno inicial > 0. InformacionGenetica != NULL
        POST: Comienza a seguir un gen */
        SeguimientoGen(informacionGenetica* gen, int turno_inicial);

        /* PRE: n>0
        POST: Registra las intensidad del turno y almacena el maximo*/
        void registrarTurno(int n);

        /* PRE: turno_final>0
        POST: Termina el seguimiento del gen
        */
        void terminarSeguimiento(int turno_final);

        void iniciarCursor();
        bool avanzarCursor();
        int obtenerCursor();

        /*PRE: -
        POST: Obtiene el turno que se inicio el seguimiento
        */
        int obtenerTurnoInicial();

        /*PRE: -
        POST: Obtiene el turno que se termino de seguir el gen*/
        int obtenerTurnoFinal();

        /*PRE: -
        POST: Obtiene la cantidad de turnos que se siguio al gen
        */
        unsigned int cantidadDeRegistros();

        /*PRE: -
        POST: Devuelve la informacion genetica del gen en seguimiento
        */
        informacionGenetica* obtenerGen();

        /* PRE: -
        POST: Devuelve el valor maximo de intensidad del gen
        */
        unsigned int valorMaximo();

        /* PRE: El gen debe estar en seguimiento
        POST: Libera la memoria
        */
        ~SeguimientoGen();

    private:
        Lista<int>* intensidades;
        informacionGenetica* p_gen;
        int valor_maximo;
        int turno_inicial;
        int turno_final;
};
